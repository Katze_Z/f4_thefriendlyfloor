

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!--[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]-->



<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://www.hadiko.de/">
    <img src="images/F4.png" alt="Logo" width="200" height="100">
  </a>

  <h3 align="center">Welcome to the  floor F4 :smile:</heart> </h3>

  <p align="center">
    Here is the wiki of the floor F4!
    <br />
    <br />
    <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">Don't click here</a>
    ·
    <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">Don't click here</a>
    ·
    <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">Don't click here</a>
  </p>




<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#greetings">Greetings</a>
      <ul>
        <li><a href="#introduction">Introduction</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Newbie giude</a>
      <ul>
        <li><a href="#prerequisites">Room</a></li>
        <li><a href="#installation">Kitchen :pizza:</a></li>
        <li><a href="#installation">Bath room :shower:</a></li>
        <li><a href="#installation">Toilet :restroom:</a></li>
        <li><a href="#installation">Trash duty</a></li>
        <li><a href="#26">Selling ice cream :icecream:</a></li>
      </ul>
    </li>
    <li>
    <a href="#usage">Events</a>
     <ul>
        <li><a href="#prerequisites">Floor dinner</a></li>
        <li><a href="#installation">Floor meeting</a></li>
        <li><a href="#installation">Tour de chambre!!! :tropical_drink:</a></li>
     </ul>
    </li>
    <li>
    <a href="#usage">Governing</a>
     <ul>
        <li><a href="#prerequisites">Floor speaker</a></li>
        <li><a href="#installation">Finance minister</a></li>
     </ul>
    </li>
    <li><a href="#contributing">Alumnus</a></li>

</details>



<!-- ABOUT THE PROJECT -->
## **1. Greetings**
Welcome to the floor F4! Here you can find some useful information that help you get to know this floor!

### **Introduction**

blablablaba



<!-- GETTING STARTED -->
## **2. Newbie giude**

This is an example

[Magic link](https://www.youtube.com/watch?v=dQw4w9WgXcQ)

### **2.1 Room**
room

### **2.2 Kitchen**
kitchen

### **2.3 Bath room**
Bath room

### **2.4 Toilet**
Toilet

### **2.5 Trash duty**
Trash duty

### <h3 id="26">**2.6 Selling ice cream**</h3> 
We have a loooooong tradition of selling ice cream to get others some cool and refreshing druing the hot summer time. Meanwhile we can get some profit form it which will go to the floor bank.

Here some explanation for the once who never Sold ice cream: 

- someone will Ring in the kitchen or at your door

- the Person **stays at the entrance door** (during the corona time) and tells you what they want 
(all sorts of icecream are hanging next to the door) 

- you get the icecream in the kitchen, the price is written on the different drawers 

- the person gives you the money 

- you go back into the kitchen and you make a mark for each 50 Cents you got 
(let's say you got 2.50€ than you put 5 marks next to your room number) 

- you take the money with you and once in a while the Finanzer (Moritz) is going to get the money from you. 

If you want to eat an ice cream, no problem, just put the marks for yourself :)



<!-- Events-->
## **3. Events**

_blabla_

### **3.1 Floor dinner**

floor dinner

### **3.2 Floor meeting**
fm

### **3.3 Tour de Chambre**
TdeC

<!-- Governing-->
## **4. Governing**

_blabla_

### **4.1 Floor speaker**

floor dinner

### **4.2 Finance minister**
fm


<!-- Alumnus-->
## **5. Alumnus**


asdasdasdasd


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Source
Project Link: [https://gitlab.com/Katze_Z/f4_thefriendlyfloor](https://gitlab.com/Katze_Z/f4_thefriendlyfloor)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Img Shields](https://shields.io)
* [Choose an Open Source License](https://choosealicense.com)
* [GitHub Pages](https://pages.github.com)
* [Animate.css](https://daneden.github.io/animate.css)
* [Loaders.css](https://connoratherton.com/loaders)
* [Slick Carousel](https://kenwheeler.github.io/slick)
* [Smooth Scroll](https://github.com/cferdinandi/smooth-scroll)
* [Sticky Kit](http://leafo.net/sticky-kit)
* [JVectorMap](http://jvectormap.com)
* [Font Awesome](https://fontawesome.com)





<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png
